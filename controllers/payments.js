module.exports = function(app){    
    app.get('/payments', function(req, res){
        console.log('recebida a requisicao na payments na porta 3000')
        res.send('OK');
    })

    app.post('/payments/payment', function(req, res){
        var payment = req.body;
        
        payment.status = 'criado';
        payment.data = new Date;
        
        console.log('Processing new payment:');
        console.log(payment);

        var connection = app.persistencia.connectionFactory;
        var pagamentoDao = new app.persistencia.PagamentoDao(connection);

        pagamentoDao.salva(payment, function(error, resultado){
            console.log('pagamento criado');
        });

        res.send('Cheguei nos pagamentos');
        res.json(payment);
    })
}
